package MainPackage;
import java.io.File;
import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;


public class DataManagementWindow {
	private Shell shell;
	private String selectedProjectDirectory = null;
	private List list;

    public DataManagementWindow(Display display) {
        shell = new Shell(display);

        createWindow();

        while (!shell.isDisposed()) {
          if (!display.readAndDispatch()) {
            display.sleep();
          }
        }
    }
    
    public class OpenDialog extends Dialog {
        Object result;
        String text;
                
        public OpenDialog (Shell parent, int style) {
                super (parent, style);
        }
        public OpenDialog (Shell parent) {
                this (parent, 0); // your default style bits go here (not the Shell's style bits)
        }
        public Object open () {
                Shell parent = getParent();
                final Shell shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
                shell.setText("Enter Project Name");
                // Your code goes here (widget creation, set result, etc).
                
                Display display = parent.getDisplay();
                //shell.setLayout(new FillLayout());
                
                int screenWidth = Display.getDefault().getBounds().width;
                int screenHeight = Display.getDefault().getBounds().height;
                int windowWidth = 250;
                int windowHeight = 120;
                
                shell.setSize(windowWidth, windowHeight);
                shell.setLocation((screenWidth - windowWidth)/2, (screenHeight - windowHeight)/2);shell.open();
                
                Label label = new Label(shell, SWT.LEFT);
                label.setText("Project Name");
                label.setBounds(16, 18, 80, 24);
                
                final Text nameField = new Text(shell, SWT.BORDER);
                nameField.setText("New Project");
                nameField.setBounds(100, 16, 100, 24);
                
                Button accept = new Button(shell, SWT.PUSH);
                accept.setText("Okay");
                accept.setBounds(80, 56, 80, 28);
                
                accept.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						text = nameField.getText();
						shell.close();
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {}	
                });
                
                //shell.pack();
                
                while (!shell.isDisposed()) {
                        if (!display.readAndDispatch()) display.sleep();
                }
                return text;
        }
 }
    
    private void createToolbar() {
    	Device dev = shell.getDisplay();
    	Image startIcon = null;
    	Image addIcon = null;
    	Image deleteIcon = null;
    	Image settingsIcon = null;

        try {
        	startIcon = new Image(dev, "src/main/resources/start.png");
        	addIcon = new Image(dev, "src/main/resources/new.png");
        	deleteIcon = new Image(dev, "src/main/resources/delete.png");
        	settingsIcon = new Image(dev, "src/main/resources/settings.png");

        } catch (Exception e) {
            System.out.println("Cannot load images");
            System.out.println(e.getMessage());
            System.exit(1);
        }
        
        GridData gridData = new GridData();
        gridData.horizontalAlignment = GridData.FILL;
        gridData.grabExcessHorizontalSpace = true;
        
        ToolBar toolBar = new ToolBar(shell, SWT.FLAT);
        toolBar.setLayoutData(gridData); 

        ToolItem start = new ToolItem(toolBar, SWT.PUSH);
        new ToolItem(toolBar, SWT.SEPARATOR);
        ToolItem add = new ToolItem(toolBar, SWT.PUSH);
        ToolItem delete = new ToolItem(toolBar, SWT.PUSH);
        new ToolItem(toolBar, SWT.SEPARATOR);
        ToolItem settings = new ToolItem(toolBar, SWT.PUSH);
        
        start.setImage(startIcon);
        add.setImage(addIcon);
        delete.setImage(deleteIcon);
        settings.setImage(settingsIcon);
         
        start.setToolTipText("Launch project");
        add.setToolTipText("Create a new project");
        delete.setToolTipText("Delete project");
        settings.setToolTipText("Settings...");
        
        start.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				Display display = shell.getDisplay();
				shell.dispose();
				new EditorWindow(display, selectedProjectDirectory);
			}

			public void widgetDefaultSelected(SelectionEvent e) {}
        });
        
        add.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				OpenDialog dialog = new OpenDialog(shell);
				String newProjectName = (String)dialog.open();
				
				if (newProjectName == null) return;
				
				File newProjectDirectory = new File("./exampleData/" + newProjectName );
				try {
					newProjectDirectory.createNewFile();
				} catch (IOException exception) {
					exception.printStackTrace();
				}
				listProjects();
			}

			public void widgetDefaultSelected(SelectionEvent e) {}
        });
        
        delete.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				File project = new File("./exampleData/" + selectedProjectDirectory );
				project.delete();
				listProjects();
			}

			public void widgetDefaultSelected(SelectionEvent e) {}
        });
        

        toolBar.pack();
    }

    private void listProjects() {
     // Directory path here
    	list.removeAll();
    	
        String path = "./exampleData"; 
       
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles(); 
       
        for (File file : listOfFiles)  {
        	if (file.isFile())  {
        		list.add(file.getName());
        	}
        }
    }

    public void createWindow() {

        shell.setText("Project Manager");
        
        Device dev = shell.getDisplay();
        shell.setImage(new Image(dev, "src/main/resources/app.png"));
        
        GridLayout gridLayout = new GridLayout();
        gridLayout.numColumns = 1;
        gridLayout.marginWidth = 0;
        gridLayout.marginHeight = 0;
        gridLayout.verticalSpacing = 0;
        shell.setLayout(gridLayout);
        
        GridData gridData_textArea = new GridData();
        gridData_textArea.horizontalAlignment = GridData.FILL;
        gridData_textArea.grabExcessHorizontalSpace = true;
        gridData_textArea.verticalAlignment = GridData.FILL;
        gridData_textArea.grabExcessVerticalSpace = true;
        
        createToolbar();
        
        list = new List(shell, SWT.BORDER);
        listProjects();

        list.addListener(SWT.Selection, new Listener () {
            public void handleEvent (Event e) {
                String[] items = list.getSelection();
                if (items.length > 0)
                	selectedProjectDirectory = items[0];
            }
        });
        
        list.setLayoutData(gridData_textArea);
        shell.pack();
        
		int screenWidth = Display.getDefault().getBounds().width;
        int screenHeight = Display.getDefault().getBounds().height;
        int windowWidth = 300;
        int windowHeight = 500;
        
        shell.setSize(windowWidth, windowHeight);
        shell.setLocation((screenWidth - windowWidth)/2, (screenHeight - windowHeight)/2);
        shell.open();
    }
}
