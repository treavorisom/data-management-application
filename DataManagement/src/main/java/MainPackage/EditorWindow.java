package MainPackage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

public class EditorWindow {
	private Shell shell;
	private String projectFilename;
	private Text textEditor;

    public EditorWindow(Display display, String projectFilename) {
    	this.projectFilename = projectFilename;
        this.shell = new Shell(display);

        createWindow();

        while (!shell.isDisposed()) {
          if (!display.readAndDispatch()) {
            display.sleep();
          }
        }
    }
    
    private void createToolbar() {
    	Device dev = shell.getDisplay();
    	Image startIcon = null;
    	Image saveIcon = null;
    	Image openIcon = null;
    	Image compileIcon = null;
    	Image settingsIcon = null;

        try {
        	startIcon = new Image(dev, "src/main/resources/start.png");
        	saveIcon = new Image(dev, "src/main/resources/save.png");
        	openIcon = new Image(dev, "src/main/resources/open.png");
        	compileIcon = new Image(dev, "src/main/resources/compile.png");
        	settingsIcon = new Image(dev, "src/main/resources/settings.png");

        } catch (Exception e) {
            System.out.println("Cannot load icon images in: src/main/resources");
            System.out.println(e.getMessage());
            System.exit(1);
        }
        
        GridData gridData = new GridData();
        gridData.horizontalAlignment = GridData.FILL;
        gridData.horizontalSpan = 1;
        gridData.grabExcessHorizontalSpace = true;
        
        ToolBar toolBar = new ToolBar(shell, SWT.FLAT);
        toolBar.setLayoutData(gridData);

        ToolItem run = new ToolItem(toolBar, SWT.PUSH);
        ToolItem compile = new ToolItem(toolBar, SWT.PUSH);
        new ToolItem(toolBar, SWT.SEPARATOR);
        ToolItem save = new ToolItem(toolBar, SWT.PUSH);
        ToolItem load = new ToolItem(toolBar, SWT.PUSH);
        new ToolItem(toolBar, SWT.SEPARATOR);
        ToolItem settings = new ToolItem(toolBar, SWT.PUSH);
         
        run.setImage(startIcon);
        compile.setImage(compileIcon);
        save.setImage(saveIcon);
        load.setImage(openIcon);
        settings.setImage(settingsIcon);
        
        run.setToolTipText("Compile and launch");
        compile.setToolTipText("Compile");
        save.setToolTipText("Save");
        load.setToolTipText("Load");
        settings.setToolTipText("Settings...");
        
        run.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				Compiler compiler = new Compiler();
				try {
					int[] program = compiler.compileBytecode(textEditor.getText());
					VirtualMachine vm = new VirtualMachine();
					vm.loadProgram(program);
					vm.run();
				} catch (Exception exception) {
					System.out.println(exception.getMessage());
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {}
        });
        
        compile.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				Compiler compiler = new Compiler();
				try {
					compiler.compileBytecode(textEditor.getText());
				} catch (Exception exception) {
					System.out.println(exception.getMessage());
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {}
        });
        
        save.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				saveFile();
			}

			public void widgetDefaultSelected(SelectionEvent e) {}
        });
        
        load.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				Display display = shell.getDisplay();
				shell.dispose();
				new DataManagementWindow(display);
			}

			public void widgetDefaultSelected(SelectionEvent e) {}
        });
        
        toolBar.pack();
        
    }
    
    private void loadFile() {
    	String fullPath = "./exampleData/" + projectFilename;
    	
    	File file = new File(fullPath);
    	String fileText = "";
    	
    	Charset charset = Charset.forName("US-ASCII");
    	try (BufferedReader reader = Files.newBufferedReader(file.toPath(), charset)) {
    	    String line = null;
    	    while ((line = reader.readLine()) != null) {
    	    	fileText += line + "\n";
    	    }
    	} catch (IOException x) {
    	    System.err.format("IOException: %s%n", x);
    	}
    	
    	textEditor.setText(fileText);
    }
    
    private void saveFile() {
    	String fullPath = "./exampleData/" + projectFilename;
    	
    	File file = new File(fullPath);
    	
    	Charset charset = Charset.forName("US-ASCII");
    	String s = textEditor.getText();
    	try (BufferedWriter writer = Files.newBufferedWriter(file.toPath(), charset)) {
    	    writer.write(s, 0, s.length());
    	} catch (IOException x) {
    	    System.err.format("IOException: %s%n", x);
    	}
    }

    public void createWindow() {
    	shell.setText("Project Editor: " + projectFilename);
        
        shell.setImage(new Image(shell.getDisplay(), "src/main/resources/app.png"));
        
        GridLayout gridLayout = new GridLayout();
        gridLayout.numColumns = 1;
        gridLayout.marginWidth = 0;
        gridLayout.marginHeight = 0;
        gridLayout.verticalSpacing = 0;
        shell.setLayout(gridLayout);
        
    	createToolbar();	
	    	
		 GridData gridData_textArea = new GridData();
		 gridData_textArea.horizontalAlignment = GridData.FILL;
		 gridData_textArea.grabExcessHorizontalSpace = true;
		 gridData_textArea.verticalAlignment = GridData.FILL;
		 gridData_textArea.grabExcessVerticalSpace = true;
        textEditor = new Text(shell, SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI | SWT.BORDER);
        textEditor.setLayoutData(gridData_textArea);
        textEditor.setText("");

        shell.pack();
        
		int screenWidth = Display.getDefault().getBounds().width;
        int screenHeight = Display.getDefault().getBounds().height;
        int windowWidth = 800;
        int windowHeight = 600;
        
        shell.setSize(windowWidth, windowHeight);
        shell.setLocation((screenWidth - windowWidth)/2, (screenHeight - windowHeight)/2);
        shell.open();
        
        loadFile();
    }
}
