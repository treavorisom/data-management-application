package MainPackage;

public class VirtualMachine {
	private static int REGISTER_SIZE = 256;
	private static int STACK_SIZE = 256;
	
	private int[] register = new int[REGISTER_SIZE];
	
	private int[] program;
	private int programCounter = -1;
	
	private int[] stack = new int[STACK_SIZE];
	private int stackCounter = 0;
	
	public void loadProgram(int[] program) {
		this.program = program;
	}
	
	private int fetch() {
		return program[++programCounter];
	}
	
	private void dumpRegister() {
		System.out.print("<----- REGISTER DUMP ----->"); 
		for (int i = 0; i < register.length; i++) {
			if (i % 32 == 0)
				System.out.print("\n");
			
			System.out.print(register[i] + " ");
		}
		System.out.println("\n<----- REGISTER DUMP ----->"); 
	}
	
	private void push(int value) {
		stack[stackCounter++] = value;
	}
	
	private int peek() {
		return stack[stackCounter - 1];
	}
	
	private int pop() {
		return stack[--stackCounter];
	}
	
	private void clear() {
		stackCounter = 0;
	}
	
	public void run() {
		while (programCounter < program.length) {
			switch (fetch()) {
				case END: {
					System.out.println("END at " + (programCounter)); 
					
					dumpRegister();
					return;
				}
				case PUT: {
					push(fetch());
					
					//System.out.println("PUT");
					break;
				}
				case DUP: {
					push(peek());
					
					//System.out.println("DUP");
					break;
				}
				case POP: {
					pop();
					
					//System.out.println("POP");
					break;
				}
				case CLR: {
					clear();
					
					//System.out.println("CLR");
					break;
				}
				case SET: { // value, dest -> value
					int dest = pop();
					int value = peek();
					
					register[dest] = value;
					
					//System.out.println("SET");
					break;
				}
				case REF: {
					push(register[pop()]);
					
					//System.out.println("REF");
					break;
				}
				case MOV: {
					int dest   = pop();
					int source = pop();
					
					register[dest] = register[source];
					
					//System.out.println("MOV");
					break;
				}
				case INC: {
					int a = pop();
					push(++a);
					
					//System.out.println("INC");
					break;
				}
				case DEC: {
					int a = pop();
					push(--a);
					
					//System.out.println("DEC");
					break;
				}
				case ADD: {
					int a = pop();
					int b = pop();
					
					push(a + b);
					
					//System.out.println("ADD");
					break;
				}
				case SUB: {
					int a = pop();
					int b = pop();
					
					push(a - b);
					
					//System.out.println("SUB");
					break;
				}
				case MUL: {
					int a = pop();
					int b = pop();
					
					push(a * b);
					
					//System.out.println("MUL");
					break;
				}
				case DIV: {
					int a = pop();
					int b = pop();
					
					push(a / b);
					
					//System.out.println("DIV");
					break;
				}
				case JMP: {
					programCounter = pop() - 1;
					
					//System.out.println("JMP " + programCounter + 1);
					break;
				}
				
				
				case CON: {
					int condition = pop();
					int address = pop();
					
					if (condition == 1)
						programCounter = address - 1;
					
					//System.out.println("CON " + programCounter + 1);
					break;
				}
				case EQU: {
					int a = pop();
					int b = pop();
					if (a == b)
						push(1);
					else
						push(0);
					
					//System.out.println("EQU");
					break;
				}
				case GEQ: {
					int a = pop();
					int b = pop();
					if (a >= b)
						push(1);
					else
						push(0);
					
					//System.out.println("GEQ");
					break;
				}
				case LEQ: {
					int a = pop();
					int b = pop();
					if (a <= b)
						push(1);
					else
						push(0);
					
					//System.out.println("LEQ");
					break;
				}
				case GRE: {
					int a = pop();
					int b = pop();
					if (a > b)
						push(1);
					else
						push(0);
					
					//System.out.println("GRE");
					break;
				}
				case LES: {
					int a = pop();
					int b = pop();
					if (a < b)
						push(1);
					else
						push(0);
					
					//System.out.println("LES");
					break;
				}
				case INV: {
					int a = pop();
					if (a == 0)
						push(1);
					else
						push(0);
					
					//System.out.println("INV");
					break;
				}
				case NEG: {
					int a = pop();
					push(-a);
					
					//System.out.println("NEG");
					break;
				}
				
				case IMPORT: {
					int dest = pop();
					int length = pop();
					
					for (int i = 0; i < length; i++)
						register[dest + i] = fetch();
					
					//System.out.println("IMPORT");
					break;
				}
				case PRINT_INT: {
					int integer = pop();

					System.out.print(integer);
					
					//System.out.println("PRINT_INT");
					break;
				}
				case PRINT_CHAR: {
					int character = pop();

					System.out.print((char)character);
					
					//System.out.println("PRINT_CHAR");
					break;
				}
				case PRINT_STR: {
					int dest = pop();
					
					while (register[dest] != 0)
						System.out.print((char)register[dest++]);
					
					//System.out.println("PRINT_STR");
					break;
				}
				default: {
					//System.out.println("default");
					break;
				}
			}
		}
	}
	
	static final int END = 0;
	
	static final int PUT = 1;
	static final int DUP = 2;
	static final int POP = 3;
	static final int CLR = 4;
	
	static final int SET = 5;
	static final int REF = 6;
	
	static final int MOV = 7;
	
	static final int INC = 8;
	static final int DEC = 9;	
	static final int ADD = 10;
	static final int SUB = 11;
	static final int MUL = 12;
	static final int DIV = 13;
	
	static final int JMP = 14;
	static final int CON = 15;
	static final int EQU = 16; // =
	static final int GEQ = 17; // >=
	static final int LEQ = 18; // <=
	static final int GRE = 19; // >
	static final int LES = 20; // <
	static final int INV = 21; // !
	static final int NEG = 22; // -
	
	static final int IMPORT = 100;
	static final int PRINT_INT = 101;
	static final int PRINT_CHAR = 102;
	static final int PRINT_STR = 103;
}
