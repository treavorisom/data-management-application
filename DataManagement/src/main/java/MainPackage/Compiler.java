package MainPackage;

import java.util.ArrayList;
import java.util.List;

public class Compiler {	
	public int[] compileBytecode(String source) throws Exception {
		String[] tokens = source.split("\\s");
		List<String> sanitizedTokens = new ArrayList<String>();
		for (String token : tokens)
			if (!token.isEmpty())
				sanitizedTokens.add(token);
		
		tokens = new String[sanitizedTokens.size()];
		for (int i = 0; i < sanitizedTokens.size(); i++)
			tokens[i] = sanitizedTokens.get(i);
		
		int[] byteCode = new int[tokens.length];
		
		for (int i = 0; i < tokens.length; i++) {
			     if (tokens[i].equals("END")) byteCode[i] = VirtualMachine.END;
			else if (tokens[i].equals("PUT")) byteCode[i] = VirtualMachine.PUT;
			else if (tokens[i].equals("DUP")) byteCode[i] = VirtualMachine.DUP;
			else if (tokens[i].equals("POP")) byteCode[i] = VirtualMachine.POP;
			else if (tokens[i].equals("CLR")) byteCode[i] = VirtualMachine.CLR;
			else if (tokens[i].equals("SET")) byteCode[i] = VirtualMachine.SET;
			else if (tokens[i].equals("REF")) byteCode[i] = VirtualMachine.REF;
			else if (tokens[i].equals("MOV")) byteCode[i] = VirtualMachine.MOV;
			else if (tokens[i].equals("INC")) byteCode[i] = VirtualMachine.INC;
			else if (tokens[i].equals("DEC")) byteCode[i] = VirtualMachine.DEC;
			else if (tokens[i].equals("ADD")) byteCode[i] = VirtualMachine.ADD;
			else if (tokens[i].equals("SUB")) byteCode[i] = VirtualMachine.SUB;
			else if (tokens[i].equals("MUL")) byteCode[i] = VirtualMachine.MUL;
			else if (tokens[i].equals("DIV")) byteCode[i] = VirtualMachine.DIV;
			else if (tokens[i].equals("JMP")) byteCode[i] = VirtualMachine.JMP;  
			else if (tokens[i].equals("CON")) byteCode[i] = VirtualMachine.CON;
			else if (tokens[i].equals("EQU")) byteCode[i] = VirtualMachine.EQU;
			else if (tokens[i].equals("GEQ")) byteCode[i] = VirtualMachine.GEQ;
			else if (tokens[i].equals("LEQ")) byteCode[i] = VirtualMachine.LEQ;
			else if (tokens[i].equals("GRE")) byteCode[i] = VirtualMachine.GRE;
			else if (tokens[i].equals("LES")) byteCode[i] = VirtualMachine.LES;
			else if (tokens[i].equals("INV")) byteCode[i] = VirtualMachine.INV;
			else if (tokens[i].equals("NEG")) byteCode[i] = VirtualMachine.NEG;
			else if (tokens[i].equals("IMPORT")) byteCode[i] = VirtualMachine.IMPORT;
			else if (tokens[i].equals("PRINT_INT")) byteCode[i] = VirtualMachine.PRINT_INT;
			else if (tokens[i].equals("PRINT_CHAR")) byteCode[i] = VirtualMachine.PRINT_CHAR;
			else if (tokens[i].equals("PRINT_STR")) byteCode[i] = VirtualMachine.PRINT_STR;
			else if (tokens[i].length() > 1 && tokens[i].charAt(0) == '\'') byteCode[i] = (char)tokens[i].charAt(1);
			else {
				try {
					byteCode[i] = Integer.parseInt(tokens[i]);
				}
				catch (NumberFormatException e) {
					throw new Exception("Compile Error: Invalid opcode at index " + i);
				}
			}
		}
		
		return byteCode;
	}
}
