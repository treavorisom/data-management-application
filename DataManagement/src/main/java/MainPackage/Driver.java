package MainPackage;
import org.eclipse.swt.widgets.Display;

public class Driver {
    public static void main(String[] args) {
        Display display = new Display();
        
        new DataManagementWindow(display);
        
        display.dispose();
    }
}