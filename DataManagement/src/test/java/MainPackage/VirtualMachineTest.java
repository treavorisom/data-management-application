package MainPackage;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class VirtualMachineTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		VirtualMachine vm = new VirtualMachine();
		
		int[] someOpcodes = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0};
		
		vm.loadProgram(someOpcodes);
		
		try {
			vm.run();
		} catch (Exception e) {
			fail("Virtual machine failed at runtime :(");
		}
	}

}
