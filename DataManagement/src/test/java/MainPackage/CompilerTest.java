package MainPackage;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CompilerTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void test() {
		Compiler compiler = new Compiler();
		String trivialSource = "END PUT DUP POP CLR SET REF MOV INC DEC ADD SUB MUL DIV JMP CON EQU GEQ LEQ GRE LES INV NEG";
		
		int[] compiledCode;
		try {
			compiledCode = compiler.compileBytecode(trivialSource);
			
			for (int i = 0; i < compiledCode.length; i++)
			assertEquals(i, compiledCode[i]);
			
		} catch (Exception e) {
			fail();
		}
	}

}
